import React from 'react'
import AddNewCar from '../screens/addcar/AddNewCar'
import AddMultipleCar from '../screens/addcar/AddMultipleCar'

function renderScreen(name) {
    console.log("renderScreen::" + name)
    if (name === "Vehicle") {
        return <AddNewCar />

    } else if (name === "AddMultipleCar") {
        return <AddMultipleCar />
    }
}

export default props =>
    <div>
        {
            renderScreen(props.screenName.split(' ').join(''))
        }
    </div>