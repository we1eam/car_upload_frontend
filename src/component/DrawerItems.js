import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import SendIcon from '@material-ui/icons/Send';
import ExpandLess from '../icons/ic_up_arrow.svg';
import ExpandMore from '../icons/ic_down_arrow.svg';
import StarBorder from '@material-ui/icons/StarBorder';
import DrawerListItem from './DrawerListItem'

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    marginLeft: 'auto',
    marginRight: 'auto',
    border: '1px solid black',
    display: 'flex',
    justifyContent: 'center'
  },
  textStyle: {
    fontSize: '12px',
    textTransform: 'capitalize',
    color: 'black',
    textStyle: 'bold',
    fontFamily: 'verdana'
  },
  nestedTextStyle: {
    fontSize: '12px',
    textTransform: 'capitalize',
    color: 'grey',
    textStyle: 'bold',
    fontFamily: 'verdana',
    display: 'flex',
    marginLeft: 'auto',
    marginRight: '0'
  },
  iconSize: {
    height: 10,
    width: 10
  }
}));


export default props => {
  const classes = useStyles();
  const [selectedIndex, setSelectedIndex] = React.useState(0)


  const handleListItemClick = (event, index) => {
    if (index === 0) {
      props.onHandle("Dashboard")
    }

    if (index === 1) {
      props.onHandle("Vehicle");
    }

    if (index === 2) {
      props.onHandle("Customers")
    }

    if (index === 3) {
      props.onHandle("Bookings")
    }

    if (index === 4) {
      props.onHandle("Billings")
    }

    if (index === 5) {
      props.onHandle("Manage")

    }

    if (index === 6) {
      props.onHandle("Support")
    }

    if (index === 7) {
      props.onHandle("Help")
    }

    if (index === 8) {
      props.onHandle("Settings")
    }


    setSelectedIndex(index)
  }

  // const handleUserProfileClick = () => {
  //   setUserProfileOpen(!openUserProfile);
  // };

  // const handleCarProfileClick = () => {
  //   setCarProfileOpen(!openCarProfile)
  // }

  return (
    <List
      component="nav"
      aria-labelledby="nested-list-subheader"

      className={classes.root}>
      {/*---------------------------------------- DashBoard -------------------------------------*/}
      <DrawerListItem text="Dashboard" icon={<SendIcon />}
        handleItemClick={(event) => handleListItemClick(event, 0)}
        index={0}
        itemSelectedIndex={selectedIndex} />

      {/* End Dashboard */}

      {/*---------------------------------------- Vehicle -------------------------------------*/}
      <DrawerListItem text="Vehicle" icon={<SendIcon />}
        handleItemClick={(event) => handleListItemClick(event, 1)} index={1} itemSelectedIndex={selectedIndex} />
      {/* End Vehicle */}

      {/*---------------------------------------- Customers -------------------------------------*/}
      <DrawerListItem text="Customers" icon={<SendIcon />}
        handleItemClick={(event) => handleListItemClick(event, 2)} index={2} itemSelectedIndex={selectedIndex} />
      {/* End Customers */}

      {/*---------------------------------------- Bookings -------------------------------------*/}
      <DrawerListItem text="Bookings" icon={<SendIcon />}
        handleItemClick={(event) => handleListItemClick(event, 3)} index={3} itemSelectedIndex={selectedIndex} />
      {/* End Bookings */}

      {/*---------------------------------------- Billings -------------------------------------*/}
      <DrawerListItem text="Billings" icon={<SendIcon />}
        handleItemClick={(event) => handleListItemClick(event, 4)} index={4} itemSelectedIndex={selectedIndex} />
      {/* End Billings */}

      {/*---------------------------------------- Manage -------------------------------------*/}
      <DrawerListItem text="Manage" icon={<SendIcon />}
        handleItemClick={(event) => handleListItemClick(event, 5)} index={5} itemSelectedIndex={selectedIndex} />
      {/* End Manage */}

      {/*---------------------------------------- Support -------------------------------------*/}
      <DrawerListItem text="Customers" icon={<SendIcon />}
        handleItemClick={(event) => handleListItemClick(event, 6)} index={6} itemSelectedIndex={selectedIndex} />
      {/* End Support */}

      {/* --------------------------------------------------Help------------------------------------------------- */}
      <DrawerListItem text="Help" icon={<InboxIcon />}
        handleItemClick={(event) => handleListItemClick(event, 7)} index={7} itemSelectedIndex={selectedIndex} />
      {/* End Help */}

      {/* -------------------------------------------------Settings------------------------------------------------ */}
      <DrawerListItem text="Settings" icon={<InboxIcon />}
        handleItemClick={(event) => handleListItemClick(event, 8)} index={8} itemSelectedIndex={selectedIndex} />
      {/* End Settings */}
    </List >
  );
}
