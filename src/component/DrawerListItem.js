import React from 'react'
import ListItem from '@material-ui/core/ListItem'
import { ListItemText, ListItemIcon, makeStyles, Button } from '@material-ui/core'
import Typography from '@material-ui/core/Typography'

const styles = makeStyles({
    listItemText: {
        fontSize: 12,
        textTransform: 'capitalize',
        fontFamily: 'verdana',
        fontStyle: 'bold'
    },
    listItemSelectedColor: {
        fontSize: 12,
        textTransform: 'capitalize',
        fontFamily: 'verdana',
        fontStyle: 'bold',
        color: 'purple'
    },
    listItemUnSelectedColor: {
        fontSize: 12,
        textTransform: 'capitalize',
        fontFamily: 'verdana',
        fontStyle: 'bold',
        color: 'black'
    }
})

export default function DrawerListItem(props) {
    const classes = styles();
    const color = props.index === props.itemSelectedIndex ? '#9B4EE4' : 'black';
    // ListItem onselected background => selected={props.index === props.itemSelectedIndex}
    return <ListItem button onClick={props.handleItemClick}>
        <ListItemIcon>
            {props.icon}
        </ListItemIcon>

        <Typography noWrap className={classes.listItemText} style={{ color }}>
            {props.text}
        </Typography>



    </ListItem>
}
