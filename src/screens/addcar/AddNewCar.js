import React from 'react'
import Radio from '@material-ui/core/Radio'
import Container from '@material-ui/core/Container'
import LoadScreen from '../../component/LoadScreen'
import Box from '@material-ui/core/Box'



export default class AddNewCar extends React.Component {
    state = {
        screenName: ''
    }

    loadScreen = (event) => {
        console.log("loadScreen:" + event.target.value)
        this.setState({
            screenName: event.target.value
        })
    }

    render() {
        const padding1 = {
            padding: '25px 0px 25px 50px'
        }

        const padding2 = {
            paddingLeft: 50
        }

        const padding3 = {
            paddingLeft: 10
        }

        const boldFontWeight = {
            fontWeight: 'bold'
        }

        const grey = {
            color: 'grey'
        }

        const purple = {
            color: '#9B4EE4'
        }

        const black = {
            color: 'black'
        }

        const fontSize10 = {
            fontSize: 10
        }

        const fontSize12 = {
            fontSize: 12
        }

        const fontSize16 = {
            fontSize: 16
        }

        const fontFamily = {
            fontFamily: 'verdana'
        }

        const greyBackground = {
            background: "#F7F7FC"
        }


        return (

            <div>
                <div style={{ ...greyBackground, ...padding1 }}>
                    <span style={{ ...purple, ...fontFamily, ...fontSize16, ...greyBackground }}>Add Vehicle</span>
                    <span style={{ ...purple, ...fontFamily, ...fontSize16, ...greyBackground }}>Add Vehicle</span>
                </div>
                <div style={{ paddingTop: 20, paddingBottom: 20 }}>
                    <span style={{ ...purple, ...fontFamily, ...fontSize16, ...padding1 }}>Add Car Registration Number</span>
                </div>
                <div style={{ paddingTop: 20 }}>
                    <span style={{ ...black, ...fontFamily, ...padding2 }}>Add Car</span>
                    <div style={{ display: 'flex', paddingLeft: 40 }}>
                        <Box>
                            <Radio disableRipple checked={this.state.screenName === 'AddSingleCar'} onChange={this.loadScreen} value="AddSingleCar" /><label style={{ ...fontSize12, ...grey }}>Add Single Car</label>
                        </Box>
                        <Box ml={1}>
                            <Radio disableRipple checked={this.state.screenName === 'AddMultipleCar'} onChange={this.loadScreen} value="AddMultipleCar" /><label style={{ ...fontSize12, ...grey }}>Add Multiple Car</label>
                        </Box>
                    </div>

                </div>
                <div style={{ paddingTop: 30, paddingLeft: 50 }} >
                    <LoadScreen screenName={this.state.screenName} />

                </div>
            </div >


        )
    }
}