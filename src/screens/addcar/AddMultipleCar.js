import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import ExpandMore from '../../icons/ic_down_arrow.svg'
import AddIcon from '../../icons/ic_add_button.svg'
import InfoIcon from '../../icons/ic_information.svg'

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1
    },
    paper: {
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    labelStyle: {
        fontSize: 12,
        color: 'grey',
        fontWeight: 'bold',
    },
    gridBackground: {
        border: '1px grey solid',
        borderRadius: 25,
        fontSize: 12,
        width: 240,
        paddingTop: 6,
        paddingBottom: 6,
        paddingLeft: 10,
        paddingRight: 10,
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center'

    },
    inputTypeStyle: {
        width: 240,
        border: '1px grey solid',
        borderRadius: 25,
        fontSize: 12,
        padding: 10
    },
    submitBtnStyle: {
        width: 240,
        border: '1px #9B4EE4 solid',
        background: '#9B4EE4',
        color: 'white',
        borderRadius: 25,
        fontSize: 10,
        padding: 10,
        boxShadow: '1px 1px 4px grey'
    },
    marginTop4: {
        marginTop: 4
    },
    arrowSize: {
        width: 10,
        height: 10
    },
    addIconSize: {
        width: 32,
        height: 32,
        marginLeft: 20
    },
    infoIconSize: {
        width: 12,
        height: 12,
        marginLeft: 10
    },
    centerItems: { display: 'flex', alignItems: 'center', paddingTop: 2 }
}));

export default function AddMultipleCar() {
    const classes = useStyles()
    return (
        <div className={classes.root} >

            <Grid container spacing={3} alignItems="flex-start" justify="flex-start" direction="row">
                <Grid item xs={3}>
                    <label className={classes.labelStyle}>Select Company</label>
                </Grid>
                <Grid item xs={3}>
                    <label className={classes.labelStyle}>Add Or Select Group</label>
                </Grid>
            </Grid>
            <Grid container spacing={3} alignItems="flex-start" justify="flex-start" direction="row" >
                <Grid item xs={3}>
                    <div className={classes.gridBackground}>
                        <label>Select Company </label>
                        <img src={ExpandMore} className={classes.arrowSize} />
                    </div>

                </Grid>
                <Grid item xs={3}>
                    <div className={classes.gridBackground}>
                        <label>Add Or Select Group</label>
                        <img src={ExpandMore} className={classes.arrowSize} />
                    </div>

                </Grid>
            </Grid>
            <Grid container spacing={10} alignItems="flex-start" justify="flex-start" direction="row" className={classes.marginTop4}>
                <Grid item xs={6}>
                    <label className={classes.labelStyle}>Enter Car Registration Numbers</label>
                </Grid>
                <Grid item xs={6} style={{ paddingLeft: 130 }}>
                    <label className={classes.labelStyle} style={{ color: 'orange' }}>Upload CSV File</label>
                    <img src={InfoIcon} className={classes.infoIconSize} />
                </Grid>
            </Grid>
            <Grid container spacing={3} alignItems="flex-start" justify="flex-start" direction="row">
                <Grid item xs={3}>
                    <div>
                        <input type="text" placeholder="Enter Car Registration Numbers" className={classes.inputTypeStyle} />
                    </div>

                </Grid>
                <Grid item xs={3}>
                    <div >
                        <input type="text" placeholder="Enter Car Registration Numbers" className={classes.inputTypeStyle} />
                    </div>

                </Grid>
                <Grid item xs={3}>
                    <div className={classes.centerItems}>
                        <input type="text" placeholder="Enter Car Registration Numbers" className={classes.inputTypeStyle} />
                        <img src={AddIcon} className={classes.addIconSize} />
                    </div>

                </Grid>
            </Grid>
            <Grid container spacing={2} alignItems="flex-end" justify="flex-end" direction="row">
                <Grid item xs={6}>
                    <div style={{ marginTop: 20, marginLeft: 10 }}>
                        <input type="submit" placeholder="Enter Car Registration Numbers" className={classes.submitBtnStyle} value="RETRIEVE CAR SPECIFICATIONS" />
                    </div>

                </Grid>
            </Grid>
        </div >
    )

}