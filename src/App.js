import React, { useState } from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/IconButton';
import Container from '@material-ui/core/Container';
import Link from '@material-ui/core/Link';
import Logo from './icons/ic_weone_logo.svg';
import NotificationsIcon from './icons/ic_notification_active.svg';
import Hamburger from './icons/ic_hamburger.svg'
import DrawerItems from './component/DrawerItems';
import { Avatar, ListItemIcon } from '@material-ui/core';
import { BrowserRouter } from 'react-router-dom'
import LoadScreen from './component/LoadScreen';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const drawerWidth = 180;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    background: 'white'
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
    background: '#ffffff'
  },
  toolbarIcon: {
    height: '10px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0 18px',
    margin: '30px',
    background: 'white'
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  toolBarTitle: {
    flexGrow: 1,
    fontSize: '18px'
  },
  title: {
    flexGrow: 1,
    fontSize: '14px'
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    height: '100vh',
    width: '100%'
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
  },
  drawerMarginTop: {
    marginTop: 20
  }
}));

export default function App() {
  const classes = useStyles();
  const [open, setOpen] = useState(true);
  const [screenName, setScreenName] = useState("Dashboard");
  const [toolBarTitle, setToolBarTitle] = useState("Dashboard")

  const handleScreen = (screen) => {
    if (screen === '') {
      return
    }
    setScreenName(screen);
    setToolBarTitle(screen)
  }
  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };

  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);
  return (
    <BrowserRouter>
      <div className={classes.root}>
        <CssBaseline />
        {/*----------------------------------------- Toolbar --------------------------------------------*/}
        <AppBar position="absolute" className={clsx(classes.appBar, open && classes.appBarShift)}>
          <Toolbar className={classes.toolbar}>

            <IconButton
              edge="start"
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              className={clsx(classes.menuButton, open && classes.menuButtonHidden)}>
              <img src={Hamburger} />
            </IconButton>

            <Typography color="textPrimary" noWrap className={classes.toolBarTitle}>
              {toolBarTitle}
            </Typography>

            <Button color="inherit" onClick={() => handleScreen("Find A Car")}>
              <Typography color="textPrimary" noWrap className={classes.title}>
                Find A Car
              </Typography>
            </Button>
            <Button color="inherit" onClick={() => handleScreen("Your Car")}>
              <Typography color="textPrimary" noWrap className={classes.title}>
                Your Car
              </Typography>
            </Button>

            <IconButton color="inherit">
              <img src={NotificationsIcon} />
            </IconButton>

            <IconButton color="inherit">
              <Avatar>N</Avatar>
            </IconButton>


          </Toolbar>
        </AppBar>
        {/*----------------------------------------- End Toolbar --------------------------------------------*/}

        {/*----------------------------------------- Drawer --------------------------------------------*/}
        <Drawer
          variant="permanent"
          classes={{
            paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
          }}
          style={{ backgroundColor: 'white' }}
          open={open}>

          <div>
            <center>
              <img src={Logo} style={{ height: '56px', padding: '12px', justifyContent: 'center' }} onClick={handleDrawerClose} />
            </center>
          </div>

          {/*-------------------------------------------Start DrawerList ----------------------------- */}
          <List className={classes.drawerMarginTop}><DrawerItems onHandle={handleScreen} /></List>
          {/*-------------------------------------------End DrawerList ----------------------------- */}

        </Drawer>
        {/*----------------------------------------- End Drawer --------------------------------------------*/}

        <main className={classes.content}>
          <div className={classes.appBarSpacer} />
          {/* <Container maxWidth="lg" className={classes.container}>

            <LoadScreen screenName={screenName} />
          </Container> */}
          <div>
            {/* //push to master */}
            <LoadScreen screenName={screenName} />
          </div>
        </main>

      </div>

    </BrowserRouter >
  );
}